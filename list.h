/********************	list.h  *****************/
/******************** 	1999.6.13   *************/

#ifndef _LIST_H
#define _LIST_H

class List
{
 private:
	void ** items;
	int buffer_length;
	int count;
 public:
	List();
	~List();
	void add_item(void * item);
	void * get_item(int handle);
	int reset_item(int handle,void * item);
	int search_item(void * item);
	int delete_item(int handle);
	void insert_item(int handle, void * item);
	void delete_all();
	int get_size();
	int get_items(void * buffer);
	int meet_end(int handle);
};

class Dictionary
{
 private:
	List * names;
	List * values;
 public:
	Dictionary();
	~Dictionary();
	void add_item(char * name,void * value);
	int have_item(char * name);
	int search_item(char * name);
	void * get_value(char* name);
	int get_size();
	void * get_value(int i);
	char * get_name(int i);
	void set_value(int i,void * value);
	void delete_item(int i);
	void display_items();
};

#endif










