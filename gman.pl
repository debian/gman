#!/usr/bin/perl
use strict(all);

my $cgi_path = "/cgi-bin/gman/gman.pl";
my $man2html = "man2html -M $cgi_path";
my $path = `man -w @ARGV`;
my $tmp_file = "/tmp/gman.$ARGV[1].$ARGV[0]";


if (!&file_exist($path)) {
	my $name = lc($ARGV[1]);
	$path = `man -w $ARGV[0] $name`;
#	print "check point 0.9\n";
}
if (!&file_exist($path)) {
	print <<end_of_line;
Content-type: text/html

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<HTML><HEAD>
<TITLE>404 Not Found</TITLE>
</HEAD><BODY>
<H1>Not Found</H1>
The requested man page $ARGV[1]($ARGV[0]) was not found on this server.<P>
</BODY></HTML>
end_of_line
	die;
}
my $page;
if ($path =~ /gz$/) {
	my $file = `gzip -cd $path`;
	open (TEMPFILE,">$tmp_file");
	print TEMPFILE $file;
	close (TEMPFILE);
	$page = `$man2html $tmp_file`;
	`rm $tmp_file`;
} else {
	$page = `$man2html $path`;
}

$page =~ s/This document was created by\s*<A HREF=\"(http:\/\/.*)\">man2html<\/A>,\s*using the manual pages.<BR>/This document was created by
<A HREF=\"$1?1+man2html\">man2html<\/A> for <A HREF=\"$1?1+gman\">gman<\/A>,
using the manual pages.<BR>/;
print $page;
#print `cat /home/wxk/src/gtk/gman.html`;

sub file_exist {
	open (FILE,$_[0]);
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
		$atime,$mtime,$ctime,$blksize,$blocks)	= stat(FILE);
	close(FILE);

#	print "check point 0.5, size = $size, $_[0]\n";
	if ($size == 0 or $size eq null) {
		return 0;
	} else {
		return 1;
	}
}
