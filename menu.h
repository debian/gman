/********************* menu.h *********************/

#ifndef _MENU_H
#define _MENU_H
#include "task.h"
#include <gtk/gtk.h>

void init_main_window(GtkWidget *);

extern GtkWidget * clist;
extern GtkWidget * clist3;

extern List * man_paths_to_be_load;
extern GtkTooltips * tooltips;
extern Dictionary * man_paths;
extern char * *man_items_buffer;
extern int man_items_count;
extern char *keyword;

extern GtkWidget *search_button;
extern GtkWidget *stop_button;

#endif

