/********************** modified from t2.c *******************/
/********************* kman.h ********************************/
#include "list.h"

#ifndef	_KMAN_H
#define _KMAN_H

#define MAX_MAN_SECTION 15
#define BUFFER_SIZE 200

class ManPath;
class ManItem;

class ManPath
{
private:
	char *	path_name;
	List *	section[MAX_MAN_SECTION]; //1:2:3:4:5:6:7:8:9:tcl:n:l:p:o
private:
	int LoadManSubPath(char *);
public:
	ManPath(char * path_name);
	~ManPath();
	int LoadManPath(char *);
	const char * GetPath();
	int GetSize(int section_ID);
	int GetItems(int section_ID, ManItem **buffer);
};

class ManItem
{
public:
	ManItem(ManPath *, char * file_name);
	~ManItem();
	char * 	get_display_name(char * buffer);
	char  * 	get_section_name(char * buffer);
 int		get_section_ID();
	void *	get_man_path();
	 void 	active_man_page();	
private:	
	ManPath	* man_path;
	char *	file_name;
	char * display_name;
	char * 	section_name;
	int 	section_ID;
};

//int ItemMatch(char * name);
int man_item_compare(ManItem **,ManItem **);

#endif /* _KMAN_H */


