#include <stdio.h>
#include "context.h"

main()
{
	int i;
	AppContext context(NULL);
	context.set_value("test",(void*)1234);
	context.set_value("v_size",(void*)1024);
	context.restore_default("test");
	context.display_values();
	i = (int)context.get_value("v_size");
	printf("v_size = %d, h_size = %d, test = %d",i,context.get_value("h_size"),context.get_value("test"));
}

