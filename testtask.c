#include "task.h"
#include <stdio.h>
#include <unistd.h>

int task_runb(int state, void * data);
int task_runa(int state, void * data);

Task *a,*b;
void main()
{
	TaskGroup * task_group;
	task_group = task_group_new();
	b = task_new(task_group,0.5,task_runb,"b");
	a = task_new(task_group,0.4,task_runa,"a");
	task_set_active(b);usleep(15000);
	//task_set_active(a);
	//task_set_active(b);
		sleep(4);
}

int task_runa(int state, void* data)
{
	static int i;
	int j,k;
	if(state&TASK_START) 
	{
		printf("task a begain... state = %d",state);
		write(1,"task a begin to run...\n",24);
		i = 0;
		//task_set_active(b);
	}
	for(j = 0;j < 100 && i< 2000;j++,i++) {
		write(1,data,1);
	}
	usleep(1);
	return (i<2000);
}

int task_runb(int state, void* data)
{
	static int i;
	int j,k;
	if(state&TASK_START) i = 0;
	for(j = 0;j < 100 && i< 1000;j++,i++)
		write(1,data,1);
	if(i==500) task_set_active(a);
		write(1,"\n",1);
	return (i<1000);
}

