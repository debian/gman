Summary: Gtk+ front-end for man, a good replacment for xman
Name: gman
Version: 0.9.3
Release: 1
Copyright: GPL
Group: Applications/System
Source: http://homex.coolconnect.com/user/xkwang/gman/gman-0.9.3.tar.gz
BuildRoot: /var/tmp/%{name}-buildroot

%description 

Gman is a front-end for the original man page system. The most basic
job of gman is to build a database for all the man pages and display
them (or part of them) as a list. When user click on a item in the
list, gman will launch a new window (xterm, cxterm, gv, netscape
browser, etc.) and call the traditional man system to display the man
page to the user.

It is simple but useful, especialy for the Linux newbies who have
difficalty in finding information with traditional man system.

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/man/man1
mkdir -p $RPM_BUILD_ROOT/var/www/cgi-bin/gman

install -s -m 755 gman $RPM_BUILD_ROOT/usr/bin/gman
install -m 755 gman.pl $RPM_BUILD_ROOT/usr/bin/gman.cgi
install -m 644 gman.1x $RPM_BUILD_ROOT/usr/man/man1/gman.1x
install -m 755 gman.pl $RPM_BUILD_ROOT/var/www/cgi-bin/gman/gman.pl

%clean
#rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README ChangeLog AUTHORS COPYING

/usr/bin/gman
/usr/man/man1/gman.1x.gz
/usr/bin/gman.cgi
/var/www/cgi-bin/gman/gman.pl

%changelog
* Sun May 18 2002 Wang Xinkai <aakwxk at hotmail.com>
- minor update for RedHat 7.x
* Mon Mar 26 2001 Wang Xinkai <aakwxk at hotmail.com>
- first build
