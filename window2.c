/******************** departed from menu.c ***************/
/******************** window2.c **************************/

#include <stdio.h>
#include <gtk/gtk.h>
#include "menu.h"
#include "list.h"
#include "util.h"
#include "mandata.h"
#include "taskfunc.h"

static int window2_button_callback(GtkWidget *w, gpointer data);
static int window2_delete_event_callback(GtkWidget *w, GdkEvent *event,gpointer data);
static int clist2_select_callback(GtkWidget *w,int row, int column, GdkEvent *event,gpointer data);
static void entry2_activate_callback(GtkWidget *,gpointer);
static void entry2_changed_callback(GtkWidget *,gpointer);

int signal_window2_change; //used by window2_button_callback() to decide whether to take action or not
GtkWidget * window2 = NULL; 	//edit paths window

GtkWidget * clist2 = NULL;
int clist2_selected_row;

GtkWidget*
create_window2 ()
{
	GtkWidget *window2;
	GtkWidget *hbox3;
	GtkWidget *vbox1;
	GtkWidget *clist2;
	GtkWidget *swindow;
	GtkWidget *hbox4;
	GtkWidget *button1;
	GtkWidget *button2;
	GtkWidget *vbox2;
	GtkWidget *vbox3;
	GtkWidget *button3;
	GtkWidget *button4;
	GtkWidget *button5;
	GtkWidget *entry2;

	gchar *titles[3] = { "a", "Path", "Items" };

	window2 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	//	GTK_WIDGET_UNSET_FLAGS (window2, GTK_CAN_FOCUS);
	gtk_object_set_data (GTK_OBJECT (window2), "window2", window2);
	gtk_window_set_title (GTK_WINDOW (window2), "edit man paths");
	gtk_window_set_policy (GTK_WINDOW (window2), FALSE, TRUE, FALSE);
	gtk_widget_set_usize(window2,300,220);

	gtk_signal_connect(GTK_OBJECT(window2), "delete_event", 
					   GTK_SIGNAL_FUNC(window2_delete_event_callback), 
					   (void*)1);

	hbox3 = gtk_hbox_new (FALSE, 10);
	gtk_object_set_data (GTK_OBJECT (window2), "hbox3", hbox3);
	gtk_widget_show (hbox3);
	gtk_container_add (GTK_CONTAINER (window2), hbox3);
	gtk_container_border_width (GTK_CONTAINER (hbox3), 10);
	
	vbox1 = gtk_vbox_new (FALSE, 10);
	gtk_object_set_data (GTK_OBJECT (window2), "vbox1", vbox1);
	gtk_widget_show (vbox1);
	gtk_box_pack_start (GTK_BOX (hbox3), vbox1, TRUE, TRUE, 0);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (window2), "vbox3", vbox1);
	gtk_widget_show (vbox3);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox3, TRUE, TRUE, 0);

    clist2 = gtk_clist_new_with_titles( 3, titles);
	::clist2 = clist2;
	swindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swindow),
									GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);

	//GTK_WIDGET_UNSET_FLAGS (swindow, GTK_CAN_FOCUS);

	gtk_clist_set_column_width(GTK_CLIST(clist2),0,10);
	gtk_clist_set_column_width(GTK_CLIST(clist2),1,130);
	gtk_clist_set_column_justification(GTK_CLIST(clist2),0,GTK_JUSTIFY_CENTER);
	gtk_clist_set_column_justification(GTK_CLIST(clist2),2,GTK_JUSTIFY_RIGHT);
	gtk_clist_set_selection_mode(GTK_CLIST(clist2),GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_passive(GTK_CLIST(clist2));
	gtk_container_add (GTK_CONTAINER (swindow), clist2);
	gtk_signal_connect(GTK_OBJECT(clist2),"select_row",GTK_SIGNAL_FUNC(clist2_select_callback),(void*)1);
	gtk_signal_connect(GTK_OBJECT(clist2),"unselect_row",GTK_SIGNAL_FUNC(clist2_select_callback),(void*)0);

	gtk_object_set_data (GTK_OBJECT (window2), "clist2", clist2);
	gtk_widget_show (clist2);
	gtk_widget_show (swindow);
	gtk_box_pack_start (GTK_BOX (vbox3), swindow, TRUE, TRUE, 0);

	entry2 = gtk_entry_new_with_max_length (200);
	gtk_object_set_data (GTK_OBJECT (window2), "entry2", entry2);
	gtk_widget_show (entry2);
	gtk_box_pack_start (GTK_BOX (vbox3), entry2, FALSE, FALSE, 0);
	gtk_signal_connect(GTK_OBJECT(entry2),"activate",GTK_SIGNAL_FUNC(entry2_activate_callback),NULL);
	gtk_signal_connect(GTK_OBJECT(entry2),"changed",GTK_SIGNAL_FUNC(entry2_changed_callback),NULL);
	gtk_tooltips_set_tip (tooltips, entry2, "Input new path here", NULL);


	hbox4 = gtk_hbox_new (TRUE, 15);
	gtk_object_set_data (GTK_OBJECT (window2), "hbox4", hbox4);
	gtk_widget_show (hbox4);
	gtk_box_pack_end (GTK_BOX (vbox1), hbox4, FALSE, TRUE, 0);
	
	button1 = gtk_button_new_with_label ("OK");
	gtk_object_set_data (GTK_OBJECT (window2), "button1", button1);
	gtk_widget_show (button1);
	gtk_box_pack_start (GTK_BOX (hbox4), button1, TRUE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(button1),"clicked",(GtkSignalFunc)window2_button_callback,(void*)1);
	GTK_WIDGET_SET_FLAGS (button1, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (button1);

	button2 = gtk_button_new_with_label ("apply");
	gtk_object_set_data (GTK_OBJECT (window2), "button2", button2);
	gtk_widget_show (button2);
	gtk_box_pack_start (GTK_BOX (hbox4), button2, TRUE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(button2),"clicked",(GtkSignalFunc)window2_button_callback,(void*)2);
	
	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (window2), "vbox2", vbox2);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (hbox3), vbox2, FALSE, TRUE, 0);
	
	button3 = gtk_button_new_with_label ("Add new");
	gtk_object_set_data (GTK_OBJECT (window2), "button3", button3);
	gtk_widget_show (button3);
	gtk_box_pack_start (GTK_BOX (vbox2), button3, FALSE, FALSE, 15);
	gtk_signal_connect (GTK_OBJECT(button3),"clicked",(GtkSignalFunc)window2_button_callback,(void*)3);
	gtk_widget_set_sensitive(button3,0);
	
	button4 = gtk_button_new_with_label ("Delete");
	gtk_object_set_data (GTK_OBJECT (window2), "button4", button4);
	gtk_widget_show (button4);
	gtk_box_pack_start (GTK_BOX (vbox2), button4, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(button4),"clicked",(GtkSignalFunc)window2_button_callback,(void*)4);
	gtk_widget_set_sensitive(button4,0);

	button5 = gtk_button_new_with_label ("Default");
	gtk_object_set_data (GTK_OBJECT (window2), "button5", button5);
	gtk_widget_show (button5);
	gtk_box_pack_start (GTK_BOX (vbox2), button5, FALSE, FALSE, 15);
	gtk_signal_connect (GTK_OBJECT(button5),"clicked",(GtkSignalFunc)window2_button_callback,(void*)5);
	
	button5 = gtk_button_new_with_label ("Reload");
	gtk_object_set_data (GTK_OBJECT (window2), "button6", button5);
	gtk_widget_show (button5);
	gtk_box_pack_start (GTK_BOX (vbox2), button5, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT(button5),"clicked",(GtkSignalFunc)window2_button_callback,(void*)6);

	button5 = gtk_button_new_with_label ("Reload all");
	gtk_object_set_data (GTK_OBJECT (window2), "button7", button5);
	gtk_widget_show (button5);
	gtk_box_pack_start (GTK_BOX (vbox2), button5, FALSE, FALSE, 15);
	gtk_signal_connect (GTK_OBJECT(button5),"clicked",(GtkSignalFunc)window2_button_callback,(void*)7);
	
	return window2;
}

/******************* tool functions  ***********************/
void load_clist2()
{
	int i,j;
	ManPath * p;
	gchar * clist_item[3];
	gchar buffer[100];
	j = man_paths->get_size();
	gtk_clist_clear(GTK_CLIST(clist2));
	for (i = 0;i<j;i++) {
		p = (ManPath*) man_paths->get_value(i);
		if (p) {
			if (p->active) clist_item[0] ="o";
			else clist_item[0] = "";
			clist_item[1] = man_paths->get_name(i);
			sprintf(buffer,"%d",p->GetSize(-1));
			clist_item[2] = buffer;
		}
		else {
			clist_item[0] = "o";
			clist_item[1] = man_paths->get_name(i);
			clist_item[2] = "???";
		}
		gtk_clist_append(GTK_CLIST(clist2),clist_item);
	}
}

/******************* interfaces to outside***********************/
int edit_paths_callback(GtkWidget *w, gpointer data)
{
	GtkWidget * apply_button,*delete_button,*focus;
	if (!window2) {
		window2 = create_window2();
		man_paths_to_be_load = new List;
		clist2_selected_row = -1;
	}
	apply_button = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(window2),"button2");
	gtk_widget_set_sensitive(apply_button,0);
	delete_button = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(window2),"button4");
	gtk_widget_set_sensitive(delete_button,0);
	signal_window2_change = 0;
	load_clist2();
	gtk_widget_show(window2);
	focus = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(window2),"button1");
	gtk_widget_grab_focus (focus);
	return 1;
}

/******************* call backs ***********************/
static int window2_delete_event_callback(GtkWidget *w, GdkEvent * event, gpointer data)
{
	GtkWidget * ok_button;
	ok_button = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(w),"button1");
	gtk_signal_emit_by_name(GTK_OBJECT(ok_button),"clicked");
	return 1;
}

static int window2_button_callback(GtkWidget *w, gpointer data)
{
	GtkWidget * x, *apply_button,*add_new_button;
	gchar * c, *c2;
	int i;
	int select = (int)data;
	gchar * clist_item[3];
	//	printf("point2 data = %d\n",select);
	apply_button = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(window2),"button2");
	add_new_button = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(window2),"button3");
	switch(select) {
	case 1: gtk_widget_hide(window2);		//OK
	case 2: 								//Apply
		if(signal_window2_change) {
			task_set_active(task_extract_man_data);
			task_set_active(task_add_data_to_clist);
		}
		signal_window2_change = 0;
		gtk_widget_set_sensitive(apply_button,0);
		break;
	case 3:									//Add New
		x = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(window2),"entry2");
		c = gtk_entry_get_text(GTK_ENTRY(x));
		if(!strcmp(c,"")) {
			g_print("g-man: path name can not be empty\n");
			break;
		}
		if (man_paths->have_item(c)) break;
		c2 = my_strdup(c);
		clist_item[0] = "o";
		clist_item[1] = c2;
		clist_item[2] = "???";
		gtk_clist_append(GTK_CLIST(clist2),clist_item);
		man_paths->add_item(c2,(void*)0);
		man_paths_to_be_load->add_item(c2);
		task_set_active(task_loading_man_data);
		signal_window2_change ++;
		gtk_widget_set_sensitive(apply_button,1);
		gtk_editable_select_region (GTK_EDITABLE (x), 0, strlen(c));
		break;
	case 4:									//Delete
		if (clist2_selected_row == -1) break;
		//		printf("remove %d\n",clist2_selected_row);
		i = clist2_selected_row;
		delete ((ManPath*)(man_paths->get_value(i)));
		man_paths->delete_item(i);
		gtk_clist_remove(GTK_CLIST(clist2),i);
		signal_window2_change ++;
		gtk_widget_set_sensitive(apply_button,1);
		break;
	default:
		g_print("g-man: sorry... this function not implementd yet, please wait for the future version\n");
	};
	return 1;
}

static int clist2_select_callback(GtkWidget *w,int row, int column, GdkEvent *event,gpointer data)
{
	GtkWidget * delete_button;
	delete_button = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(window2),"button4");
	if(data) {
		clist2_selected_row = row;
	}
	else {
		if (clist2_selected_row == row) clist2_selected_row = -1;
	}
	gtk_widget_set_sensitive(delete_button,clist2_selected_row != -1);
	return 0;
}

static void entry2_activate_callback(GtkWidget *w,gpointer data)
{
	GtkWidget * add_new_button;
	add_new_button = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(window2),"button3");
	//	printf("button3 = %x\n",add_new_button);
	gtk_signal_emit_by_name(GTK_OBJECT(add_new_button),"clicked");
}

static void entry2_changed_callback(GtkWidget *w,gpointer data)
{
	GtkWidget * add_new_button;
	add_new_button = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(window2),"button3");
	//	printf("button3 = %x\n",add_new_button);
	gtk_widget_set_sensitive(add_new_button,strcmp("",gtk_entry_get_text(GTK_ENTRY(w))));
}
