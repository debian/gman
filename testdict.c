/*************** testdict.c ******************/

#include "list.h"
#include <stdio.h>

void main()
{
	Dictionary * dict;
	dict = new Dictionary;
	dict->add_item("/usr/man",(void *)100);
	dict->add_item("/usr/local/man",(void * )200);
	printf("size = %d, /usr/man = %d\n",dict->get_size(),dict->get_value("/usr/local/man"));
}
