/******************** context.h ********************/

#ifndef _GMAN_H
#define _GMAN_H

#include "context.h"
#include <pthread.h>

extern pthread_mutex_t gtk_lock;
extern pthread_mutex_t context_lock;
extern pthread_mutex_t loading_man_path_lock;
extern AppContext * context;
extern int debuging;
#endif
